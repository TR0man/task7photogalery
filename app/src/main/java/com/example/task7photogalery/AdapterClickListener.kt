package com.example.task7photogalery

interface AdapterClickListener {
    fun onElementClick(position: Int)
}