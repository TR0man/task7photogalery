package com.example.task7photogalery

import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter (private val elements: List<Uri>, private val elementClickListener: AdapterClickListener) : RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {

    private var lastSelectedItem: RecyclerViewHolder? = null

    inner class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var image: ImageView? = null

        init {
            image = itemView.findViewById(R.id.ivMiniPhoto)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val elementView = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_element, parent, false)
        return RecyclerViewHolder(elementView)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {

        holder.image?.setImageURI(elements[position])
        holder.image?.setOnClickListener {
            elementClickListener.onElementClick((elements.size-1) - position)

            // можно убрать (только для декора) поставить margin="4dp" убрать android:padding="2dp"
            lastSelectedItem?.let {
                it.image?.setBackgroundColor(Color.argb(0, 0, 0, 170))
            }
            holder.image?.setBackgroundColor(Color.argb(255, 0, 0, 170))
            lastSelectedItem = holder
        }
    }

    override fun getItemCount(): Int {
        return elements.size
    }
}


// пример альтернативной длинной записи

// binding.rwMiniImageList.adapter = RecyclerViewAdapter(photoList.reversed(),
//    object : AdapterClickListener {
//        override fun onElementClick(position: Int) {
//            mainImage.setImageURI(photoList[position])
//            Toast.makeText(applicationContext, "$position", Toast.LENGTH_SHORT).show()
//        }
//    })
