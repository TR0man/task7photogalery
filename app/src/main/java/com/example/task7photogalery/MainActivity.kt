package com.example.task7photogalery

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.task7photogalery.databinding.ActivityMainBinding
import com.example.utils.Constants
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity(), AdapterClickListener {

    lateinit var binding: ActivityMainBinding
    private val mainImage by lazy { binding.ivMainPhoto }

    private var photoDirectoryPath: File? = null
    private var directoryUri: Uri? = null

    private var photoList = mutableListOf<Uri>()
    private var adapter: RecyclerViewAdapter? = null

//    lateinit var recyclerView: RecyclerView                                                       // REMOVE after TEST

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (!cameraCheck()) {
            Toast.makeText(this, getString(R.string.toast_info_no_camera_message), Toast.LENGTH_SHORT).show()
            finish()
        }
        loadPreferences()
        getSavedPhoto()

//   --- REMOVE BLOCK AFTER TEST
//        rotateImage(photoList)
//        recyclerView = binding.rwMiniImageList
//        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//        recyclerView.adapter = RecyclerViewAdapter(photoList)
//   ---------------

        adapter = RecyclerViewAdapter(photoList.reversed(),this)
        binding.rwMiniImageList.adapter = adapter
        binding.rwMiniImageList.layoutManager = LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false)

//        binding.rwMiniImageList.adapter = RecyclerViewAdapter(photoList.reversed(),this)          // для передачи this в качестве интерфейса надо реализовать методы инт-йса в активити (еще пример в Константы)
    }

    override fun onElementClick(position: Int) {
        mainImage.setImageURI(photoList[position])
        Toast.makeText(applicationContext, "$position", Toast.LENGTH_SHORT).show()
    }

    // возможно нужна проверка был ли поворот экрана в этой сессии, если был то
    // загружать фото в обратном порядке (т.к. после поворота фото выводятся в обычном порядке)
    // или вообще убрать "добавление фото в начало", а добавлять в конец
    private fun updateRecyclerViewList(newPhoto: Uri?) {
        newPhoto?.let {
            photoList.add(newPhoto)
            binding.rwMiniImageList.adapter = RecyclerViewAdapter(photoList.reversed(), this)             // reverse PhotoList for better viewing

//            adapter?.notifyItemInserted(photoList.size)
//            binding.rwMiniImageList.adapter?.notifyItemChanged(photoList.size-1)                  // вариант для авто обновления списка при отображении от начал списка до конца
        }
    }

    // получение списка сохраненных файлов в папке с моими фото
    private fun getSavedPhoto() {
        photoDirectoryPath?.let {
            val files = it.absoluteFile.listFiles()
            files?.forEach {
                photoList.add(FileProvider.getUriForFile(
                        this, BuildConfig.APPLICATION_ID + ".provider", it))
            }
            if (photoList.isNotEmpty()) {
                binding.ivMainPhoto.setImageURI(photoList.last())                                   // ADDITIONAL SET LAST IMAGE IN Main ImageView
            }
        }

        // ----- DEL BLOCK
//        photoList.forEachIndexed { index, uri ->
//            Log.d("LOG", "list["+index+"] = " + uri)
//        }
        // ----------------
    }

    override fun onResume() {
        super.onResume()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkingPermission()
        }
    }

    // проверка если камеры на устройстве (возможно убрать за ненадобностью)
    private fun cameraCheck(): Boolean {
        val packageManager = packageManager
        return packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)
    }

    // проверка необходимых разрешений для работы приложения и сохранения данных
    private fun checkingPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "Permission BAD", Toast.LENGTH_SHORT).show()           // REMOVE LATER
            ActivityCompat.requestPermissions(this, Constants.PERMISSION_LIST, Constants.PERMISSION_REQUEST_CODE)
        } else {
            Toast.makeText(this, "Permission OK", Toast.LENGTH_SHORT).show()            // REMOVE LATER
            setListeners()
        }
    }

    private fun setListeners() {
        binding.btnMakePhoto.setOnClickListener {
            createPhoto()
        }
    }

    // запуск приложения работы с камерой
    private fun createPhoto() {
        directoryUri = getAlbumStorageDirectory(this, Constants.DIRECTORY_WITH_PHOTO_NAME)
        Log.d("LOG", ">>>>>> Uri.fromFile(directoryUri) = " + directoryUri)

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, directoryUri)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivityForResult(intent, Constants.ACTIVITY_REQUEST_CODE)
    }

    //
    private fun getAlbumStorageDirectory(context: Context, albumName: String): Uri {
        if (photoDirectoryPath == null) {
            Log.d("LOG", ">>>>>> photoDirectoryPath = NULL")                               // REMOVE LATER
            photoDirectoryPath = File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), albumName)
            photoDirectoryPath?.let {
                if (!it.exists()) {
                    it.mkdirs()
                }
            }
            savePreferences(photoDirectoryPath)
        }

//        Log.d("LOG", ">>>>>> photoDirectoryPath = " + photoDirectoryPath)                   // DEL

        val file = File(photoDirectoryPath?.path + File.separator + "photo_" +
                SimpleDateFormat("ddMMy_HHmmss_SSS").format(Date()) + ".jpg")

        // ---- DEL BLOCK
//        Uri.fromFile(file)
//        Log.d("LOG", ">>>>>> Uri.fromFile(file) = " + Uri.fromFile(file))
        // --------------

        return FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", file)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Log.d("LOG", ">>>>>> REQUEST_CODE / RESULT_CODE = OK")

            mainImage.setImageURI(directoryUri)
            updateRecyclerViewList(directoryUri)

        } else {
            Log.d("LOG", ">>>>>> REQUEST_CODE = NOT OK")
        }
    }

    private fun rotateImage(list: MutableList<Uri>) {
        val tempList = mutableListOf<File>()

        photoDirectoryPath?.let {
            val files = it.absoluteFile.listFiles()
            files?.forEach {
                tempList.add(it)
            }
        }

        val uri = tempList.last()
        val exif = ExifInterface(uri.toString())
        val length = exif.getAttribute(ExifInterface.TAG_IMAGE_LENGTH)
        val width = exif.getAttribute(ExifInterface.TAG_IMAGE_WIDTH)
        val orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
        val orientation2 = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        val und = exif.getAttribute(ExifInterface.ORIENTATION_UNDEFINED.toString())
        val orientationNormal = exif.getAttribute(ExifInterface.ORIENTATION_NORMAL.toString())
        val orientation90 = exif.getAttribute(ExifInterface.ORIENTATION_ROTATE_90.toString())
        val orientation180 = exif.getAttribute(ExifInterface.ORIENTATION_ROTATE_180.toString())
        val orientation270 = exif.getAttribute(ExifInterface.ORIENTATION_ROTATE_270.toString())

        Log.d("LOG", ">>>>>> [EXIF] > FILE = $uri")
        Log.d("LOG", ">>>>>> [EXIF] > length = $length")
        Log.d("LOG", ">>>>>> [EXIF] > width = $width")

        Log.d("LOG", ">>>>>> [EXIF] > orientation = $orientation")
        Log.d("LOG", ">>>>>> [EXIF] > orientation2 = $orientation2")
        Log.d("LOG", ">>>>>> [EXIF] > UNDEFINED = $und")
        Log.d("LOG", ">>>>>> [EXIF] > orientationNormal = $orientationNormal")
        Log.d("LOG", ">>>>>> [EXIF] > orientation90 = $orientation90")
        Log.d("LOG", ">>>>>> [EXIF] > orientation180 = $orientation180")
        Log.d("LOG", ">>>>>> [EXIF] > orientation270 = $orientation270")
    }

    // получение пути из хранилища
    private fun loadPreferences() {
        val loadData = getPreferences(MODE_PRIVATE).getString(Constants.PREFERENCES_KEY, null)
        loadData?.let {
            photoDirectoryPath = File(it)
            Log.d("LOG", ">>> [PREF != NULL] > LOADED DATA = $photoDirectoryPath")                 // REMOVE
        } ?: Log.d("LOG", ">>> [PREF == NULL] > photoDirectoryPath = $photoDirectoryPath")     // REMOVE
    }

    // сохранение пути в хранилище
    private fun savePreferences(directoryPath: File?) {
        getPreferences(MODE_PRIVATE).edit().putString(Constants.PREFERENCES_KEY, directoryPath.toString()).apply()
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("LOG", ">>>>>> [onDestroy]")
//        getPreferences(MODE_PRIVATE).edit().clear().apply()                                       // FOR TEST - AFTER DELETE
    }



}