package com.example.utils

import android.Manifest
import android.widget.Toast
import com.example.task7photogalery.AdapterClickListener

class Constants {

    companion object {
        const val PREFERENCES_KEY = "path"
        const val DIRECTORY_WITH_PHOTO_NAME = "PhotoAlbum"
        const val ACTIVITY_REQUEST_CODE = 100
        const val PERMISSION_REQUEST_CODE = 200
        val PERMISSION_LIST = arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE)


    }
}

